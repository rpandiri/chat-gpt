Issues Faced with Chat-GPT:

- Unable to process images and few types of tables
- Unable to achieve continuous Conversation
- Unable to get consistent results for the same query

Things achieved with Chat-GPT:

- Able to process the pdf into chunks and feed it to the API
- Achieved answering questions based on the data fed to API
- Developed a web interface which can be used by customers to query the Data fed to API
